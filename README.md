# maker


# установка программ сборки проекта (npm)  и среды выполнения (nodejs)
установка nvm (node version manager)
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
    заново открыть терминал

установка node, npm(node process manager)
    nvm install v16.19.0
    на более новых версиях возникает ошибка 


## Сборка проекта

1. git pull
2. npm install 
3. npm run build
В случае ошибки Error: Cannot find module '@typescript-eslint/parser'
выполняем в папке проекта 
```
npm install eslint
```



Apache

## установка Centos
    sudo yum install httpd
    Перезапуск после изменения конфига
        sudo systemctl restart httpd    
    Настройки
        /etc/httpd/conf/httpd.conf

## установка Ubuntu
    sudo apt update; sudo apt install apache2
    настройка на запуск сервера после включения машины
        sudo systemctl enable apache2
    Настройки
        /etc/apache2/apache2.conf
        /etc/apache2/sites-available/000-default.conf
        /etc/apache2/mods_available/alias.conf
    Перезапуск после изменения конфига
        sudo systemctl restart apache2

узнать где лежат текущие настройки Centos
    Centos httpd -V
    Ubuntu apache2 -V

## Указать папку с контентом 
    Centos /etc/httpd/conf/httpd.conf
    Ubuntu /etc/apache2/sites-available/000-default.conf
         DocumentRoot /opt/maker/dist

## Псевдонимы для перехода на конкретную страницу из любого места - 
    Centos /etc/httpd/conf/httpd.conf
    Ubuntu /etc/apache2/mods_available/alias.conf
        <IfModule alias_module>
            Alias /task     /opt/maker/dist
            Alias /job 	    /opt/maker/dist
            Alias /job2     /opt/maker/dist
            Alias /import	/opt/maker/dist
            Alias /log	    /opt/maker/dist
            Alias /catalog	/opt/maker/dist
            Alias /config	/opt/maker/dist

## Права на доступ к контенту
    Centos /etc/httpd/conf/httpd.conf 
    Ubuntu /etc/apache2/apache2.conf    
        <Directory "/opt/maker/dist">
            AllowOverride None
            # Allow open access:
            Require all granted
        </Directory>

## Настройка разных сайтов на одном сервере
    <VirtualHost *:80>
    DocumentRoot /opt/dev2/maker/dist
    ServerName dev2
    ServerAlias dev2
    <IfModule alias_module>
        Alias /task 	  /opt/dev2/maker/dist
        Alias /job 		  /opt/dev2/maker/dist
        Alias /job2 	  /opt/dev2/maker/dist
        Alias /import	  /opt/dev2/maker/dist
        Alias /log		  /opt/dev2/maker/dist
        Alias /catalog	/opt/dev2/maker/dist
        Alias /config   /opt/dev2/maker/dist	
        Alias /doc      /opt/dev2/maker/dist	
        Alias /report   /opt/dev2/maker/dist	
    </IfModule>
    </VirtualHost>

# Для работы с буфером обмена - копирование текста и изображений
chrome://flags/#unsafely-treat-insecure-origin-as-secure
    добавить     http://10.54.9.247  
        и нажать enabled


# Доступ к АПИ google

Sample app with service account
https://codingfundas.com/how-to-read-edit-google-sheets-using-node-js/index.html

