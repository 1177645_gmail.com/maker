import { getBlobFromImageSource, convertBlobToPng, copyBlobToClipboard } from 'copy-image-clipboard'

export async function copyImageToClipboard(imageSource) {
    const blob = await getBlobFromImageSource(imageSource);
    const pngBlob = await convertBlobToPng(blob);
    await copyBlobToClipboard(pngBlob);
    return blob; 
}

export async function copyToClipboard(textToCopy) {
// Navigator clipboard api needs a secure context (https)
/*if (navigator.clipboard && window.isSecureContext) {
    await navigator.clipboard.writeText(textToCopy);
} else */{
    // Use the 'out of viewport hidden text area' trick
    const textArea = document.createElement("textarea");
    textArea.value = textToCopy;
        
    // Move textarea out of the viewport so it's not visible
    textArea.style.position = "absolute";
    textArea.style.left = "-999999px";
        
    document.body.prepend(textArea);
    textArea.select();

    try {
        document.execCommand('copy');
    } catch (error) {
        console.error(error);
    } finally {
        textArea.remove();
    }
  }
}

export function ok (fields, value) {
    if (!value) return true
    let ret = false
    for (let field of fields) {
      ret = field &&  field.toString().toLowerCase().includes(value.toLowerCase())
      if (ret) break
    }
    return ret  
  }
