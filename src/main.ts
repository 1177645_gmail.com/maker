
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index' 
import YmapPlugin from 'vue-yandex-maps';
import { initConfig } from './config/provider.js'

// 
fetch(process.env.BASE_URL + "config.json")
  .then((response) => response.json())
  .then((config) => {
    initConfig(config)

    const app = createApp(App);

    app.use(router);
    
    const settings = {
        apiKey: '',
        lang: 'ru_RU',
        coordorder: 'longlat',
        enterprise: false,
        version: '2.1'
    }
    app.use(YmapPlugin, settings);
    
    app.mount('#app');    
  })

