import { createWebHistory, createRouter } from "vue-router";
import Task from '../maker/Task.vue'
import Job from '../maker/Job.vue'
import Job2 from '../maker/Job2.vue'
import Log from '../maker/Log.vue'
import Image from '../maker/Image.vue'
import Import from '../maker/Import.vue'
import Catalog from '../maker/Catalog.vue'
import Report from '../maker/Report.vue'
import Config from '../maker/Config.vue'

const routes = [
  {
    path: "/task",
    name: "Task",
    component: Task,
  },
  {
    path: '/',
    redirect: '/task',
  },
  {
    path: "/job",
    name: "Job",
    component: Job,
  },
  {
    path: "/job2",
    name: "Job2",
    component: Job2,
  },
  {
    path: "/log",
    name: "Log",
    component: Log,
  },
  {
    path: "/image",
    name: "Image",
    component:Image,
  },
  {
    path: "/import",
    name: "Import",
    component: Import,
  },
  {
    path: "/catalog",
    name: "Catalog",
    component: Catalog,
  },
  {
    path: "/report",
    name: "Report",
    component: Report,
  },
  {
    path: "/config",
    name: "Config",
    component: Config,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;