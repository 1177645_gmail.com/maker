export let redouteProvider = null
export let tileProvider = null

export function initConfig(config)  {
    console.log(config)
    redouteProvider = config.service
    tileProvider = config.tiles
}
