﻿# maker


# установка программ сборки проекта (npm)  и среды выполнения (nodejs)
установка nvm (node version manager)
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

установка node, npm(node process manager)
    nvm install v16.19.0
    на более новых версиях возникает ошибка 


## Сборка проекта

1. git pull
2. npm install 
3. npm run build
В случае ошибки Error: Cannot find module '@typescript-eslint/parser'
выполняем в папке проекта 
```
npm install eslint
```



Apache

узнать где  лежат текущие настройки
    httpd -V
        на 236 сервере 
        /etc/httpd/conf/httpd.conf


Папка с контентом и псевдонимы для перехода на конкретную страницу из любого места - файл /etc/httpd/conf/httpd.conf

    DocumentRoot "/opt/maker/dist"
    ...
    Alias /task /opt/maker/dist
    Alias /job 	/opt/maker/dist
    Alias /job2 /opt/maker/dist
    Alias /find	/opt/maker/dist
    Alias /log	/opt/maker/dist
    Alias /catalog	/opt/maker/dist

Перезапуск web server apache
    sudo systemctl restart httpd